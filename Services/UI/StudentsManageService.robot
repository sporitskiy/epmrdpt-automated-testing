*** Settings ***
Library            String
Resource           ../../Pages/TrainingsManagePages/CurrentTrainingPages/CurrentGroupPage.robot
Resource           ../../Pages/TrainingsManagePages/CurrentTrainingPages/ParticipantsPage.robot
Resource           ../../Pages/TrainingsManagePages/CurrentTrainingPages/DistributionPage.robot
Resource           ../../Pages/TrainingsManagePages/CurrentTrainingPages/CurrentTrainingPage.robot

*** Variables ***
${1ST_STUDENT_NAME}     Sergey
${2ND_STUDENT_NAME}     Ivan
${2ND_STUDENT_NAME_CHECKBOX_IN_GROUP_ADDMANY_LOCATOR_PATTERN}     //td[contains(text(),'${2ND_STUDENT_NAME}')]/..//input
${DELETE_STUDENT_BUTTON_LOCATOR_PATTERN}     //a[contains(text(),'${2ND_STUDENT_NAME}')]/../../..//button[@title="Remove student"]
${temp_student_name}     ${EMPTY}
${2ND_STUDENT_NAME_LOCATOR_PATTERN}     //a[contains(text(),'${2ND_STUDENT_NAME}')]

*** Keywords ***
Add Participant
     Open Tab     Participants
     Click     ${SELECT_STUDENT_LOCATOR}
     Type     ${SELECT_STUDENT_FILTER_INPUT_LOCATOR}     ${1ST_STUDENT_NAME}
     Wait Until Page Does Not Contain Element     ${SEARCH_RESULT_LOCATOR}
     Wait Until Page Contains Element     ${SEARCH_RESULT_LOCATOR}
     Wait Until Element Does Not Contain     ${SEARCH_RESULT_LOCATOR}     No matches for
     ${temp_student_name}=     Get Text     ${SEARCH_RESULT_LOCATOR}
     Click     ${SEARCH_RESULT_LOCATOR}
     ${temp_student_name}=     Remove String Using Regexp     ${temp_student_name}     \\([\\@\\.\\_\\w]+\\)
     Set Test Variable     ${temp_student_name}
     Click     ${ADD_BUTTON_LOCATOR}

Add Many Participants
     Open Tab     Participants
     Click     ${ADD_MANY_PARTICIPANTS_BUTTON_LOCATOR}
     Type     ${ADDMANY_NAME_FILTER_LOCATOR}     ${2ND_STUDENT_NAME}
     Sleep     500ms
     Click     ${THIRD_CHECKBOX_IN_ADDMANY_LOCATOR}
     ${temp_student_name}=     Get Text     ${THIRD_STUDENT_IN_ADDMANY_LOCATOR}
     Set Test Variable     ${temp_student_name}
     Click     ${ADD_AND_CLOSE_BUTTON_LOCATOR}

Add Student to Group with 'Add Many'
     Click Element     ${ADD_MANY_STUDENTS_TO_GROUP_BUTTON_LOCATOR}
     Wait Until Element Is Visible     ${ADDMANY_AND_CLOSE_BUTTON_LOCATOR}
     Click Element     ${2ND_STUDENT_NAME_CHECKBOX_IN_GROUP_ADDMANY_LOCATOR_PATTERN}
     ${temp_student_name}=     Get Text     //td[contains(text(),'${2ND_STUDENT_NAME}')]
     Set Test Variable     ${temp_student_name}
     Click     ${ADDMANY_AND_CLOSE_BUTTON_LOCATOR}     0.5

Add Student from Other Training
     Click     ${OTHER_TRAINING_LINK_LOCATOR}
     Type     ${OTHER_TRAINING_FILTER_LOCATOR}     Tratata
     Click     ${OTHER_TRAINING_RESULT_LOCATOR}
     ${temp_student_name}     Get Text     ${SELECTED_FOR_ADDING_STUDENT_LOCATOR}
     Set Test Variable     ${temp_student_name}
     Click     ${ADD_STUDENT_FROM_OTHER_GROUP_BUTTON_LOCATOR}

Add Student in Distribution
     Open Tab     Distribution
     ${temp_student_name}=     Get Text     ${STUDENT_THAT_IS_NOT_IN_GROUP_LOCATOR}
     Set Test Variable     ${temp_student_name}
     Click     ${NOT_IN_GROUP_STATUS_LOCATOR}
     Select From List     ${NOT_IN_GROUP_STATUS_SELECT_LOCATOR}     ${group_name}

Add Student to group
     Click Element     ${SELECTED_FOR_ADDING_STUDENT_LOCATOR}
     Click Element     //li[text()='${student_name}']
     Click Element     ${ADD_SELECTED_STUDENT_BUTTON_LOCATOR}
     Wait Until Element Is Visible     //a[text()='${student_name}']
     Set Test Variable     ${temp_student_name}     ${student_name}

Accept Students for Training
     Open Tab     Participants
     ${students}=     Get Matching Xpath Count     ${STUDENTS_IN_PARTIPICANTS_TAB_LOCATORS}
     ${students}=     Evaluate     ${students}+1
     : FOR     ${index}     IN RANGE     1     ${students}
     \     ${status}=     Get Text     //tbody[@class="available-for-action"]/tr[${index}]//span[contains(@class, 'status-label')]
     \     Run Keyword If     '${status}' == 'New'     Change student status to Accepted     ${index}

Student is added to group
     Page Should Contain Element     //a[text()='${temp_student_name}']

Student status is
     [Arguments]     ${status}
     Open Tab     Participants
     Element Should Contain     //a[text()='${temp_student_name}']/../../..//span[@class="pane"]     ${status}

Open Test Group
     Go To     ${group_url}

Delete Student from Group
     ${temp_student_name}     Get Text     ${2ND_STUDENT_NAME_LOCATOR_PATTERN}
     Set Test Variable     ${temp_student_name}
     Click     ${DELETE_STUDENT_BUTTON_LOCATOR_PATTERN}
     Click     ${CONFIRM_DELETE_STUDENT_BUTTON_LOCATOR}

Change student status to Accepted
     [Arguments]     ${index}
     Click     //tbody[@class="available-for-action"]/tr[${index}]//span[contains(@class, 'status-label')]
     Select From List     //tbody[@class="available-for-action"]/tr[${index}]//select     Accepted for Training
     Wait for Ajax

Set Students as Finished Training
     ${students}=     Get Matching Xpath Count     ${STUDENTS_IN_GROUP_LOCATORS}
     ${students}=     Evaluate     ${students}+1
     : FOR     ${index}     IN RANGE     1     ${students}
     \     Click     //table[@id='groupParticipants']/tbody/tr[${index}]//span[text()='Training In Progress']
     \     Select From List     //table[@id='groupParticipants']/tbody/tr[${index}]//select     Training finished
