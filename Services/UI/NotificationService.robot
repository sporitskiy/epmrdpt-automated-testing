*** Settings ***
Resource           ../../Pages/MainPage.robot

*** Variables ***

*** Keywords ***
Notification is received
     Click     ${NOTIFICATIONS_BUTTON_LOCATOR}     0,3
     Page Should Contain Element     //a[@title="${training_name} > Group 1: schedule has been changed"]

Clear notifications
     Click     ${NOTIFICATIONS_BUTTON_LOCATOR}     0.3
     Click     ${READ_ALL_NOTIFICATIONS_BUTTON_LOCATOR}
