*** Settings ***
Library            String
Resource           ../../Pages/TrainingsManagePages/TrainingCreatingPage.robot
Resource           ../../Pages/TrainingsManagePages/CurrentTrainingPages/CurrentTrainingPage.robot
Resource           ../../Pages/TrainingsManagePages/TrainingsManagementPage.robot
Resource           ../../Pages/TrainingsManagePages/CurrentTrainingPages/CurrentGroupPage.robot
Resource           ../../Pages/TrainingsManagePages/CurrentTrainingPages/GroupsPage.robot

*** Variables ***
${TRAINING_SKILL}     Jedi Powers
${groups_count}     ${EMPTY}
${count}           ${EMPTY}
${group_url}       https://evbyminsd1706.minsk.epam.com/Plan/1405/Group/Index/3716
${training_url}     https://evbyminsd1706.minsk.epam.com/Plan/Edit/1405
${LOCATION_CITY}     Jedi City
${status}          ${EMPTY}
${group_name}      ${EMPTY}
${training_name}     Jedi Powers Part 1
${current_day}     ${EMPTY}

*** Keywords ***
Add Group Trainer
     Click     //span[text()='Select trainer']
     Type     //span[text()='Select trainer']/../..//div[@class="chzn-search"]/input     Yoda
     Sleep     1
     Click     //span[text()='Select trainer']/../..//ul/li[contains(@class,"active-result")]
     Click     //input[@title="Add selected trainer"]

Add Groups
     [Arguments]     ${groups_to_add}
     Open Tab     Groups
     Count Groups
     Set Test Variable     ${groups_count}     ${count}
     : FOR     ${index}     IN RANGE     ${groups_to_add}
     \     Click     ${ADD_GROUP_BUTTON_LOCATOR}
     ${groups_count}     Evaluate     ${groups_count}+${groups_to_add}

Allow self-registration
     Select Checkbox     ${ALLOW_SELF_REG_CHECKBOX_LOCATOR}

Change Date
     Click     ${START_DATE_LOCATOR}
     ${current_day}=     Get Text     ${CURRENT_DAY_LOCATOR}
     Click     ${PREVIOUS_ARROW_LOCATOR}
     Click     //table[@class='ui-datepicker-calendar']//a[text()='${current_day}']

Count Groups
     ${count}=     Get Text     ${GROUPS_COUNTER_LOCATOR}
     ${count}=     Remove String Using Regexp     ${count}     [ ]
     ${count}=     Remove String Using Regexp     ${count}     Items\\d+to\\d+from
     ${count}=     Convert To Integer     ${count}

Create New Training
     Open Training Creating
     Select From List     ${SKILL_SELECT_LOCATOR}     ${TRAINING_SKILL}
     Set Global Variable     ${TRAINING_SKILL}
     ${rand_str} =     Generate Random String     3     [UPPER]
     ${training_name}=     Catenate     ${TRAINING_SKILL}     ${rand_str}
     Set Global Variable     ${training_name}
     Type     ${NAME_INPUT_LOCATOR}     ${training_name}
     Change Date
     Type     ${PLANNED_STUDENTS_INPUT_LOCATOR}     10
     Click     ${TARGET_CITY_LOCATOR}
     Type     ${TARGET_CITY_FILTER_LOCATOR}     ${LOCATION_CITY}
     Click     ${TARGET_CITY_RESULT_LOCATOR}
     Click     ${CITY_LOCATOR}
     Type     ${CITY_FILTER_LOCATOR}     ${LOCATION_CITY}
     Click     ${CITY_RESULT_LOCATOR}
     Set Global Variable     ${LOCATION_CITY}
     Click     ${CREATE_BUTTON_LOCATOR}

Detete Group
     Open Tab     Groups
     Count Groups
     Set Test Variable     ${groups_count}     ${count}
     Click     ${X_BUTTON_LOCATOR}
     Click     ${CONFIRM_DELETE_GROUP_BUTTON_LOCATOR}
     ${groups_count}=     Evaluate     ${groups_count}-1

Find Training
     Type     ${TRAINING_SEARCH_INPUT_LOCATOR}     ${training_name}
     Click     ${APPLY_BUTTON_LOCATOR}
     Click     //a[text()='${training_name}']
     ${training_url}=     Get Location
     Set Global Variable     ${training_url}

Group Deleted
     Count Groups
     Should Be Equal     ${count}     ${groups_count}

Group Status is
     [Arguments]     ${status}
     Element Should Contain     ${GROUP_STATUS_LOCATOR}     ${status}

Groups Added
     Count Groups
     Should Be Equal     ${count}     ${groups_count}

Open First Group
     Open Tab     Groups
     ${group_name}=     Get Text     ${FIRST_GROUP_NAME_LOCATOR}
     Set Global Variable     ${group_name}
     Click Element     ${FIRST_GROUP_LINK_LOCATOR}
     Wait Until Element Is Visible     ${ADD_MANY_STUDENTS_TO_GROUP_BUTTON_LOCATOR}
     ${group_url}=     Get Location
     Set Global Variable     ${group_url}

Open Test Training
     Go To     ${training_url}

Set Group Status
     [Arguments]     ${status}
     Click     //span[@data-entity-state="${status}"]
     Click     ${CONFIRM_SET_GROUP_STATUS_BUTTON_LOCATOR}
     Wait Until Page Does Not Contain Element     //span[@data-entity-state="${status}"]

Set Training Status
     [Arguments]     ${status}
     Click     //span[@class="common-button shift-button" and contains(text(),'${status}')]
     Click     ${I_AM_SURE_BUTTON_LOCATOR}
     ${click_once_more}=     Get Matching Xpath Count     ${I_AM_SURE_BUTTON_LOCATOR}
     Run keyword if     ${click_once_more} != 0     Click     ${I_AM_SURE_BUTTON_LOCATOR}
     ${is_popup_error_displayed}=     Get Matching Xpath Count     ${TRAINING_SHOUND_CONTAIN_GROUPS_POPUP_MESSAGE_LOCATOR}
     Run Keyword If     ${is_popup_error_displayed} == 0     Wait Until Page Contains Element     //span[@class="common-button shift-button activeButton" and contains(text(),'${status}')]
     Run Keyword If     ${is_popup_error_displayed} != 0     Wait Until Page Does Not Contain Element     ${TRAINING_SHOUND_CONTAIN_GROUPS_POPUP_MESSAGE_LOCATOR}

Training Status is
     [Arguments]     ${status}
     Element Should Contain     ${CURRENT_STATUS_LOCATOR}     ${status}
