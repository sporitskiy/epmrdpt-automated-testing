*** Settings ***
Resource           ../../Pages/TrainingsPages/TrainingsPage.robot
Resource           ../../Pages/TrainingsPages/RegistrationPage.robot

*** Variables ***

*** Keywords ***
Cancel Training
     Open Tab     Set started
     Find and Open Training     ${training_name}
     Click     ${CANCEL_REGISTRATION_BUTTON_LOCATOR}
     Type     ${REASON_INPUT_LOCATOR}     Because
     Click     ${APPLY_CANCEL_REGISTRATION_BUTTON_LOCATOR}

Change Training Location
     [Arguments]     ${city}
     Click     ${TRAINING_LOCATION_LINK_LOCATOR}
     Type     ${TRAINING_LOCATION_FILTER_LOCATOR}     ${city}
     Click     ${TRAINING_LOCATION_RESULT_LOCATOR}

Find and Open Training
     [Arguments]     ${training_name}
     Change Training Location     ${LOCATION_CITY}
     : FOR     ${INDEX}     IN RANGE     1     10
     \     ${IsTrainingPresent}=     Get Matching Xpath Count     //a[contains(text(),'${training_name}')]
     \     Run Keyword If     ${IsTrainingPresent}==1     Exit For Loop
     \     Click     ${NEXT_ARROW_LOCATOR}
     Click     //a[contains(text(),'${training_name}')]

Register to Planned Training
     Open Tab     Planned
     Set Test Variable     ${training_name}     Jedi Powers Part 2
     Find and Open Training     ${training_name}
     Click     ${APPLY_TRAINING_BUTTON_LOCATOR}
     Click     ${CONFIRM_APPLY_BUTTON_LOCATOR}
     Click     ${POPUP_MESSAGE_LOCATOR}

Register to Staffing Training as newcomer
     #Open Tab     Set started
     Find and Open Training     ${training_name}
     Click     ${REGISTER_TO_TRAINING_BUTTON_LOCATOR}
     Type     ${LASTNAME_INPUT_LOCATOR}     ${student_lastname}
     Type     ${FIRSTNAME_INPUT_LOCATOR}     ${student_firstname}
     Type     ${PHONE_NUMBER_INPUT_LOCATOR}     123456789
     Click     ${NEXT_BUTTON_LOCATOR}
     Click     //label[text()='${TRAINING_SKILL}']/..//span[@title='Novice']
     Click     ${NEXT_BUTTON_LOCATOR}
     Select From List     ${ENGLISH_LEVEL_SELECT_LOCATOR}     Advanced
     Click     ${ADDRESS_LOCATOR}
     Type     ${ADDRESS_SEARCH_INPUT_LOCATOR}     ${LOCATION_CITY}
     Click     ${ADDRESS_SEARCH_RESULT_LOCATOR}
     Click     ${NEXT_BUTTON_LOCATOR}
     Click     ${CONFIRM_REGISTER_BUTTON_LOCATOR}

Restore Registration
     Click     ${RESTORE_REGISTRATION_BUTTON_LOCATOR}

Student status is
     [Arguments]     ${status}
     Element Should Contain     ${STUDENT_STATUS_LOCATOR}     ${status}

Trainings are displayed
     [Arguments]     ${count}
     Xpath Should Match X Times     ${TRAININS_IN_APPS_PAGE_LOCATORS}     2
