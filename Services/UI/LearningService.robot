*** Settings ***
Resource           ../../Pages/LearningPages/LearningPage.robot
Resource           ../../Pages/LearningPages/LearningGroupPage.robot
Library            String

*** Variables ***
${learning_group_url}     https://evbyminsd1706.minsk.epam.com/Attendance/Group/3563
${task_for_student_locator}     ${EMPTY}
${MARK}            9
${task_name}       Kill sith
${lesson_name}     Mastering your power
${online_task_name}     Quickly kill sith

*** Keywords ***
Find Learning Group
     Select Checkbox     ${ONLY_MY_GROUPS_CHECKBOX_LOCATOR}
     Click     ${APPLY_FILTER_BUTTON_LOCATOR}
     : FOR     ${INDEX}     IN RANGE     1     10
     \     ${IsTrainingPresent}=     Get Matching Xpath Count     //span[contains(text(),'${training_name}')]
     \     Run Keyword If     ${IsTrainingPresent}==1     Exit For Loop
     \     Click     ${NEXT_ARROW_LOCATOR}
     Click     //span[contains(text(),'${training_name}')]
     ${learning_group_url}=     Get Location
     Set Global Variable     ${learning_group_url}

Add Lesson
     Click     ${ADD_LESSON_BUTTON_LOCATOR}
     ${rand_str} =     Generate Random String     3     [UPPER]
     ${lesson_name}=     Catenate     ${lesson_name}     ${rand_str}
     Set Global Variable     ${lesson_name}
     Type     ${SUBJECT_INPUT_LOCATOR}     ${lesson_name}
     Click     ${CONFIRM_ADD_LESSON_BUTTON_LOCATOR}
     Open Tab     Attendance journal     #Temporary, because of bug, needs refreshing tab

Add Task
     Click     ${ADD_TASK_BUTTON_LOCATOR}
     ${rand_str} =     Generate Random String     3     [UPPER]
     ${task_name}     Catenate     ${task_name}     ${rand_str}
     Set Global Variable     ${task_name}
     Type     ${TASK_NAME_INPUT_LOCATOR}     ${task_name}
     Click     ${CONFIRM_ADD_TASK_BUTTON_LOCATOR}     0.5

Add Online Task
     Click     ${ADD_TASK_BUTTON_LOCATOR}
     Type     ${DURATION_INPUT_LOCATOR}     10
     ${rand_str} =     Generate Random String     3     [UPPER]
     ${online_task_name}     Catenate     ${online_task_name}     ${rand_str}
     Set Global Variable     ${online_task_name}
     Type     ${TASK_NAME_INPUT_LOCATOR}     ${online_task_name}
     Click     ${CONFIRM_ADD_TASK_BUTTON_LOCATOR}     1.5

Assign Online Task
     Find assigned task locator
     Click     ${task_for_student_locator}
     Click     ${ASSIGN_BUTTON_LOCATOR}     1

Change Lesson
     Click     //a[contains(text(),'Schedule')]     2     #Because of AJAX Bug on Page
     Click     //span[@title='${lesson_name}']     1
     Click     ${SAVE_LESSON_CHANGES_BUTTON_LOCATOR}     1
     Click     ${INFORM_STUDENTS_BUTTON_LOCATOR}     0,3
     Type     ${CHANGE_LESSON_COMMENT_INPUT_LOCATOR}     We will start at morning
     Click     ${OK_SEND_NOTIFICATION_BUTTON_LOCATOR}     0,5

Change marks visibility
     Open Learning Group
     Open Tab     Online tasks
     Unselect Checkbox     ${SHOW_MARKS_CHECKBOX_LOCATOR}

Lesson is in Calendar
     Page Should Contain Element     //span[@title='${lesson_name}']

Student can see Lesson
     Page Should Contain Element     //*[contains(text(),'${lesson_name}')]

Mark is Not Visible
     Element Should Contain     //span[text()='${online_task_name}']/../span[contains(@class,'markCell')]     --/10

Mark is Visible
     Element Should Contain     //span[text()='${online_task_name}']/../span[contains(@class,'markCell')]     ${MARK}/10

Online task is added
     Page Should Contain Element     //a[text()='${online_task_name}']

Online task status is
     [Arguments]     ${task_status}
     Element Text Should Be     ${task_for_student_locator}     ${task_status}

Onlite task is assigned to student
     Element Text Should Be     ${task_for_student_locator}     Assigned

Open Learning Group
     Go To     ${learning_group_url}

Submit Online Task
     Open Tab     Online tasks
     Open online task
     Click     //span[text()='${online_task_name}']/../..//a[contains(text(),'Submit your work')]
     Type     //span[text()='${online_task_name}']/../..//textarea     I killed this traitor, master.
     Click     //span[text()='${online_task_name}']/../..//input[@value="Submit"]     0.3

Open online task
     Click     //span[text()='${online_task_name}']

Reject online task
     Open Learning Group
     Open Tab     Online tasks
     Click Element     ${task_for_student_locator}
     Click     ${REJECT_BUTTON_LOCATOR}
     Type     ${APPROVING_TASK_COMMENT_INPUT_LOCATOR}     Bad work
     Click     ${CONFIRM_REJECT_BUTTON_LOCATOR}
     Click     ${APPROVED_BUTTON_LOCATOR}     1

Approve online task
     Open Learning Group
     Open Tab     Online tasks
     Click Element     ${task_for_student_locator}
     Click     ${CANCEL_REVIEW_BUTTON_LOCATOR}
     Click     ${APPROVE_TASK_BUTTON_LOCATOR}
     Type     ${APPROVING_TASK_COMMENT_INPUT_LOCATOR}     Good work
     Select From List By Label     ${MARK_SELECT_LOCATOR}     ${MARK}
     Click     ${CONFIRM_APPROVING_TASK_BUTTON_LOCATOR}
     Click     ${APPROVED_BUTTON_LOCATOR}     1

Send Mail to Student
     Click     //a[text()='${student_name}']/../../../td[@class='check']/label
     Click     ${SEND_MAIL_BUTTON_LOCATOR}

Task is added
     Page Should Contain Element     //*[contains(text(),'${task_name}')]

Student can see Task
     Page Should Contain Element     //*[contains(text(),'${task_name}')]

Task status is
     [Arguments]     ${status}
     Element Should Contain     //span[contains(@class,'taskName') and text()='${online_task_name}']/../span[@class="statusCell"]/span     ${status}

Find assigned task locator
     ${table_size}     Get Matching Xpath Count     //section[@class="content clearfix"]/table/tbody/tr[@class="ng-scope"]
     ${table_size}     Evaluate     ${table_size}+1
     : FOR     ${index}     IN RANGE     1     ${table_size}
     \     ${name}=     Get Text     //section[@class="content clearfix"]/table/tbody/tr[@class="ng-scope"][${index}]//a
     \     ${student_index}=     Set Variable If     '${name}'=='${student_name}'     ${index}
     \     Run Keyword If     '${name}'=='${student_name}'     Exit For Loop
     Set Global Variable     ${task_for_student_locator}     //section[@class="content clearfix"]/table/../div//tr[@class="ng-scope"][${student_index}]//span[contains(@class,'status-label')]
