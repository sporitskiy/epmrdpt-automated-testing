*** Settings ***
Library            String
Resource           ../../Pages/LoginPage.robot
Resource           ../../Pages/MainPage.robot
Resource           ../../Pages/UserPages/AccountsPage.robot

*** Variables ***
${PASSWORD}        qwerty
${TRAINER_USER_NAME}     master_yoda@jedi.kn
${INCORRECT_USER_NAME}     chubaka@vuki.pl
${INCORRECT_PASSWORD}     Arrrrrgh
${NON_FORMAT_USER_NAME}     R2D2_Robot
${student_user_name}     anakin_skywalker@padavan.sw
${student_firstname}     Junior
${student_lastname}     Padawan
${student_name}     Anakin Skywalker

*** Keywords ***
Change Password
     Open Accounts Page
     Click     ${CHANGE_PASSWORD_LINK_LOCATOR}
     Type     ${CURRENT_PASSWORD_INPUT_LOCATOR}     ${PASSWORD}
     Type     ${NEW_PASSWORD_INPUT_LOCATOR}     ${PASSWORD}
     Type     ${CONFIRM_NEW_PASSWORD_INPUT_LOCATOR}     ${PASSWORD}
     Click     ${SUBMIT_BUTTON_LOCATOR}

Create New User
     ${rand_str} =     Generate Random String     3     [UPPER]
     ${student_lastname}     Catenate     SEPARATOR=-     ${student_lastname}     ${rand_str}
     Set Global Variable     ${student_lastname}
     ${student_user_name}     Catenate     SEPARATOR=     ${student_lastname}     @jedi-academy.sw
     Set Global Variable     ${student_user_name}
     ${student_name}=     Catenate     ${student_firstname}     ${student_lastname}
     Set Global Variable     ${student_name}

Log Out
     ${url}=     Catenate     SEPARATOR=     ${URL}     /Auth/Logout
     Go To     ${url}

Logged to portal
     Element Should Be Visible     ${USER_MENU_LOCATOR}

Login as Student
     Open Login Page
     Type     ${USERNAME_INPUT_LOCATOR}     ${student_user_name}
     Type     ${PASSWORD_INPUT_LOCATOR}     ${PASSWORD}
     Click     ${SIGN_IN_BUTTON_LOCATOR}

Login as Trainer
     Open Login Page
     Type     ${USERNAME_INPUT_LOCATOR}     ${TRAINER_USER_NAME}
     Type     ${PASSWORD_INPUT_LOCATOR}     ${PASSWORD}
     Click     ${SIGN_IN_BUTTON_LOCATOR}

Login with correct account
     Login as Student

Login with incorrect account
     Click     ${SIGN_IN_MENU_BUTTON_LOCATOR}
     Type     ${USERNAME_INPUT_LOCATOR}     ${INCORRECT_USER_NAME}
     Type     ${PASSWORD_INPUT_LOCATOR}     ${INCORRECT_PASSWORD}
     Click     ${SIGN_IN_BUTTON_LOCATOR}

Register new user
     Open Login Page
     Click     ${REGISTER_LINK_LOCATOR}
     Type     ${NEW_USERNAME_INPUT_LOCATOR}     ${student_user_name}
     Type     ${PASSWORD_INPUT_LOCATOR}     ${PASSWORD}
     Click     ${TERMS_CHECKBOX_LOCATOR}
     Click     ${REGISTER_BUTTON_LOCATOR}

Reset Password
     Open Login Page
     Click     ${FORGOT_PASSWORD_LINK_LOCATOR}
     Type     ${USERNAME_INPUT_LOCATOR}     ${student_user_name}
     Click     ${RESET_PASSWORD_BUTTON_LOCATOR}

Write non-format username
     Click     ${SIGN_IN_MENU_BUTTON_LOCATOR}
     Type     ${USERNAME_INPUT_LOCATOR}     ${NON_FORMAT_USER_NAME}
     Type     ${PASSWORD_INPUT_LOCATOR}     ${INCORRECT_PASSWORD}

Check Language
     ${text}=     Get Text     ${PROFILE_COMPLETENESS_LOCATOR}
     Run Keyword If     '${text}' != 'Profile completeness:'     Change Language to English

Change Language to English
     Open User Menu
     Click     ${SETTINGS_LINK_LOCATOR}
     Select From List     ${LANGUAGE_SELECT_LOCATOR}     en-US
     Click     ${SAVE_CHANGES_BUTTON_LOCATOR}
