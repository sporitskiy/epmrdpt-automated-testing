*** Settings ***
Resource           ../../Utils/BrowserUtils.robot

*** Variables ***
${ONLY_MY_GROUPS_CHECKBOX_LOCATOR}     //input[@id='MyGroups']
${APPLY_FILTER_BUTTON_LOCATOR}     //input[@title="Apply filter"]
${NEXT_ARROW_LOCATOR}     //a[@class="next"]

*** Keywords ***
