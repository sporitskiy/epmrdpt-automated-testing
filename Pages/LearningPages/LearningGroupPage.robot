*** Settings ***
Resource           ../../Utils/BrowserUtils.robot

*** Variables ***
${ADD_TASK_BUTTON_LOCATOR}     //a[@ng-click="addTask()"]
${CONFIRM_ADD_LESSON_BUTTON_LOCATOR}     //input[@name="Subject"]/../..//input[@value="Add"]
${ADD_LESSON_BUTTON_LOCATOR}     //a[@data-action="addLesson"]
${ADD_BUTTON_LOCATOR}     //input[@value="Add"]
${SUBJECT_INPUT_LOCATOR}     //input[@name="Subject"]
${TASK_NAME_INPUT_LOCATOR}     //input[contains(@name,"ame")]
${DURATION_INPUT_LOCATOR}     //input[@name="durationValue"]
${ASSIGN_BUTTON_LOCATOR}     //input[@value="Assign"]
${CONFIRM_ADD_TASK_BUTTON_LOCATOR}     //input[@value="Add"]
${REJECT_BUTTON_LOCATOR}     //a[text()='Reject']
${CONFIRM_REJECT_BUTTON_LOCATOR}     //input[@ value="Reject"]
${APPROVING_TASK_COMMENT_INPUT_LOCATOR}     //textarea[@placeholder="Write your comments here"]
${CANCEL_REVIEW_BUTTON_LOCATOR}     //a[contains(text(),'Cancel review')]
${CONFIRM_APPROVING_TASK_BUTTON_LOCATOR}     //input[@value="Approve"]
${APPROVED_BUTTON_LOCATOR}     //a[contains(text(),'Approved')]
${APPROVE_TASK_BUTTON_LOCATOR}     //a[text()='Approve']
${MARK_SELECT_LOCATOR}     //div[@class="clearfix"]/select
${SEND_MAIL_BUTTON_LOCATOR}     //a[text()='Send mail']
${SHOW_MARKS_CHECKBOX_LOCATOR}     //label[@title="Check to hide marks from students"]/input
${OK_SEND_NOTIFICATION_BUTTON_LOCATOR}     //input[@value="OK"]
${CHANGE_LESSON_COMMENT_INPUT_LOCATOR}     //textarea[@name="comment"]
${INFORM_STUDENTS_BUTTON_LOCATOR}     //a[@title="Inform students about changes in the schedule"]
${SAVE_LESSON_CHANGES_BUTTON_LOCATOR}     //input[@value="Save"]

*** Keywords ***
