*** Settings ***
Resource           ../Utils/BrowserUtils.robot

*** Variables ***
${RESET_PASSWORD_BUTTON_LOCATOR}     //input[@class="button I restore js-forgot-password-submit"]
${USERNAME_INPUT_LOCATOR}     //input[@id='Name']
${NEW_USERNAME_INPUT_LOCATOR}     //input[@id='Email']
${PASSWORD_INPUT_LOCATOR}     //input[@id='Password']
${SIGN_IN_BUTTON_LOCATOR}     //input[@value='Sign in']
${CLOSE_SIGN_IN_FORM_BUTTON}     //span[text()='close']
${FORGOT_PASSWORD_LINK_LOCATOR}     //a[@class='link forgotPass js-forgot-password']
${REGISTER_LINK_LOCATOR}     //a[@class='link register js-get-register']
${TERMS_CHECKBOX_LOCATOR}     //*[@id='TermsAccepted']
${REGISTER_BUTTON_LOCATOR}     //*[@class='button I register']
${HOME_LINK_LOCATOR}     //a[@href="/"]
