*** Settings ***
Resource           ../../Utils/BrowserUtils.robot

*** Variables ***
${TRAINING_LOCATION_LINK_LOCATOR}     //div[@class="filterBox clearfix"]
${TRAINING_LOCATION_RESULT_LOCATOR}     //li[contains(@class,'active-result')]
${TRAINING_LOCATION_FILTER_LOCATOR}     //div[@class="chzn-search"]/input
${STUDENT_STATUS_LOCATOR}     //p[contains(text(),'Status:')]/span
${PLANNED_TAB_LOCATOR}     //a[text()='Planned']
${CONFIRM_APPLY_BUTTON_LOCATOR}     //input[@value="Apply"]
${TRAININS_IN_APPS_PAGE_LOCATORS}     //section/section/article
${REASON_INPUT_LOCATOR}     //textarea[@id='Reason']
${STARTED_TAB_LOCATOR}     //a[text()='Set started']
${50_LINK_LOCATOR}     //a[text()='50']
${APPLY_TRAINING_BUTTON_LOCATOR}     //a[@ class="button VI click register"]
${BACK_LINK_LOCATOR}     //a[@title="Back to training list"]
${NEXT_ARROW_LOCATOR}     //a[@class="next"]
${POPUP_MESSAGE_LOCATOR}     //div[contains(@class,'status-message')]
${CONFIRM_REGISTER_BUTTON_LOCATOR}     //input[@value="Register"]
${CANCEL_REGISTRATION_BUTTON_LOCATOR}     //a[text()='Cancel registration']
${APPLY_CANCEL_REGISTRATION_BUTTON_LOCATOR}     //input[@id="cancel-registration-button"]
${RESTORE_REGISTRATION_BUTTON_LOCATOR}     //input[@value="Restore registration"]
${REGISTER_TO_TRAINING_BUTTON_LOCATOR}     //a[@class="button I click register"]

*** Keywords ***
Back
     Click     ${BACK_LINK_LOCATOR}
