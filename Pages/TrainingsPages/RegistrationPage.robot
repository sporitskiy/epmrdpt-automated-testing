*** Settings ***
Resource           ../../Utils/BrowserUtils.robot

*** Variables ***
${LASTNAME_INPUT_LOCATOR}     //input[@id='User_LastName']
${FIRSTNAME_INPUT_LOCATOR}     //input[@id='User_FirstName']
${PHONE_NUMBER_INPUT_LOCATOR}     //input[@id='User_PhoneWork']
${NEXT_BUTTON_LOCATOR}     //input[@id='next']
${ENGLISH_LEVEL_SELECT_LOCATOR}     //select[@id='EnglishLevel']
${ADDRESS_SEARCH_RESULT_LOCATOR}     //li[contains(@class,'active-result')]
${ADDRESS_LOCATOR}     //span[text()='Address']
${ADDRESS_SEARCH_INPUT_LOCATOR}     //div[@class="address"]//div[@class="chzn-search"]/input
