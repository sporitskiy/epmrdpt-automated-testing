*** Settings ***
Resource           ../../Utils/BrowserUtils.robot

*** Variables ***
${CHANGE_PASSWORD_LINK_LOCATOR}     //a[@class="link hover click js-change-password"]
${NEW_PASSWORD_INPUT_LOCATOR}     //input[@id='NewPassword']
${CONFIRM_NEW_PASSWORD_INPUT_LOCATOR}     //input[@id='NewPasswordConfirm']
${SUBMIT_BUTTON_LOCATOR}     //*[@id='dialog-container']//input[@type="submit"]
${CURRENT_PASSWORD_INPUT_LOCATOR}     //*[@id='CurrentPassword']
