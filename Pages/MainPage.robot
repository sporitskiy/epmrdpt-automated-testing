*** Settings ***
Resource           ../Utils/BrowserUtils.robot

*** Variables ***
${SIGN_IN_MENU_BUTTON_LOCATOR}     //a[@class="signInBtn button I js-signIn"]
${USER_MENU_LOCATOR}     //*[@id='userBtn']
${MAIN_SECTIONS_MENU_BUTTON_LOCATOR}     //h1
${TRAININGS_LINK_LOCATOR}     //a[text()='Trainings']
${NEWS_LINK_LOCATOR}     //a[text()='News']
${ABOUT_LINK_LOCATOR}     //a[@href="/About"]
${HOME_LINK_LOCATOR}     //a[@class='topNavItem home click hover' and @href='/']
${LEARNING_LINK_LOCATOR}     //a[@href='/Learning/Home']
${TRAININGS_MANAGEMENT_LINK_LOCATOR}     //a[@href='/Plan']
${REPORTS_LINK_LOCATOR}     //a[@href='/Reports']
${GRADUATES_LINK_LOCATOR}     //a[@href='/Graduates']
${LOG_OUT_LINK_LOCATOR}     //a[@href="/Auth/Logout"]
${ACCOUNTS_LINK_LOCATOR}     //*[@id='userDropDownMenu']/a[text()='Accounts']
${APPLICATIONS_LINK_LOCATOR}     //*[@id='userDropDownMenu']/a[text()='Applications']
${NOTIFICATIONS_BUTTON_LOCATOR}     //a[@id='notificationBtn']
${READ_ALL_NOTIFICATIONS_BUTTON_LOCATOR}     //a[@id='unreadAll']
${PROFILE_COMPLETENESS_LOCATOR}     //article[@class="meterBox clearfix"]/p
${SETTINGS_LINK_LOCATOR}     //*[@id='userDropDownMenu']/a[4]
${LANGUAGE_SELECT_LOCATOR}     //select[@id='Locale']
${SAVE_CHANGES_BUTTON_LOCATOR}     //input[@type="submit"]

*** Keywords ***
Open About Page
     Open Sections Menu
     Click     ${ABOUT_LINK_LOCATOR}

Open Accounts Page
     Open User Menu
     Click     ${ACCOUNTS_LINK_LOCATOR}

Open Graduates Page
     Open Sections Menu
     Click     ${GRADUATES_LINK_LOCATOR}
     Wait Until Element Is Not Visible     //img[@src="/Content/ico/preloader.gif"]     1m

Open Home Page
     Open Sections Menu
     Click     ${HOME_LINK_LOCATOR}

Open Learning Page
     Open Sections Menu
     Click     ${LEARNING_LINK_LOCATOR}

Open Login Page
     Click     ${SIGN_IN_MENU_BUTTON_LOCATOR}

Open News Page
     Open Sections Menu
     Click     ${NEWS_LINK_LOCATOR}

Open Reports Page
     Open Sections Menu
     Click     ${REPORTS_LINK_LOCATOR}

Open Sections Menu
     Click     ${MAIN_SECTIONS_MENU_BUTTON_LOCATOR}
     Sleep     1

Open Training Management Page
     Open Sections Menu
     Click     ${TRAININGS_MANAGEMENT_LINK_LOCATOR}

Open Trainings Page
     Open Sections Menu
     Click     ${TRAININGS_LINK_LOCATOR}

Open User Menu
     Click     ${USER_MENU_LOCATOR}
     Sleep     1

Open Applications Page
     Open User Menu
     Click     ${APPLICATIONS_LINK_LOCATOR}
