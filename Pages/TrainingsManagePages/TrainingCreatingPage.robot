*** Settings ***
Resource           ../../Utils/BrowserUtils.robot

*** Variables ***
${SKILL_SELECT_LOCATOR}     //select[@id='Plan_TypeId']
${NAME_INPUT_LOCATOR}     //input[@id='Plan_Name']
${PLANNED_STUDENTS_INPUT_LOCATOR}     //input[@id='Plan_GraduationCount']
${CITY_LOCATOR}     //label[text()='City']/../..//a[@class='chzn-single chzn-default']/div
${TARGET_CITY_LOCATOR}     //label[text()='Target city']/../..//a[@class='chzn-single chzn-default']/div
${CREATE_BUTTON_LOCATOR}     //input[@type='submit' and @value='Create']
${TAGRET_CITY_LIST_LOCATOR}     //label[text()='Target city']/../..//ul[@class='chzn-results']
${CITY_LIST_LOCATOR}     //label[text()='City']/../..//ul[@class='chzn-results']
${TO_TRAININGS_MANAGEMENT_LINK_LOCATOR}     //a[@class="common-link left-orientation" and @href="/Plan"]
${ALLOW_SELF_REG_CHECKBOX_LOCATOR}     //*[@id='Plan_StaffingOpen']
${TARGET_CITY_FILTER_LOCATOR}     //label[text()='Target city']/../..//div[@class='chzn-search']/input
${CITY_FILTER_LOCATOR}     //label[text()='City']/../..//div[@class='chzn-search']/input
${TARGET_CITY_RESULT_LOCATOR}     //label[text()='Target city']/../..//ul[@class='chzn-results']/li[contains(@class,"active-result")]
${CITY_RESULT_LOCATOR}     //label[text()='City']/../..//ul[@class='chzn-results']/li[contains(@class,"active-result")]
${CURRENT_DAY_LOCATOR}     //table[@class='ui-datepicker-calendar']//td[contains(@class,'ui-datepicker-current-day')]/a
${START_DATE_LOCATOR}     //input[@id='Plan_StartDate']
${PREVIOUS_ARROW_LOCATOR}     //a[@title="Prev"]

*** Keywords ***
Return to Training Management Page
     Click     ${TO_TRAININGS_MANAGEMENT_LINK_LOCATOR}
