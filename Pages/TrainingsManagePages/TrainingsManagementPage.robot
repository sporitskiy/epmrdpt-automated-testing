*** Settings ***
Resource           ../../Utils/BrowserUtils.robot

*** Variables ***
${CREATE_TRAINING_BUTTON_LOCATOR}     //a[@href='/Plan/Create']
${TRAINING_SEARCH_INPUT_LOCATOR}     //input[@placeholder="Search by training name"]
${APPLY_BUTTON_LOCATOR}     //input[@title="Apply filter"]

*** Keywords ***
Open Training Creating
     Click     ${CREATE_TRAINING_BUTTON_LOCATOR}
