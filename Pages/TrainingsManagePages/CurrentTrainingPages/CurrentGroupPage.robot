*** Settings ***
Resource           ../../../Utils/BrowserUtils.robot

*** Variables ***
${GROUP_STATUS_LOCATOR}     //label[@for="Group_Status"]/../span
${CONFIRM_SET_GROUP_STATUS_BUTTON_LOCATOR}     //form//input[@type="submit"]
${ADD_STUDENT_FROM_OTHER_GROUP_BUTTON_LOCATOR}     //input[@ title="Add selected student"]
${OTHER_TRAINING_LINK_LOCATOR}     //a[@class="chzn-single"]/span[contains(text(),'Jedi Powers')]
${OTHER_TRAINING_FILTER_LOCATOR}     //div[@class="chznBox normal student-box"]/div[1]//div[@class="chzn-search"]/input
${OTHER_TRAINING_RESULT_LOCATOR}     //div[@class="chznBox normal student-box"]/div[1]//li[contains(@class,"active-result")]
${ADD_MANY_STUDENTS_TO_GROUP_BUTTON_LOCATOR}     //input[@value="Add many"]
${ADDMANY_AND_CLOSE_BUTTON_LOCATOR}     //input[@class="button I js-addManyWithClose"]
${SELECTED_FOR_ADDING_STUDENT_LOCATOR}     //div[@id='student_chzn']/a/span
${GROUP_NAME_LOCATOR}     //h1[@class="mainTitle"]/span
${STUDENTS_IN_GROUP_LOCATORS}     //table[@id='groupParticipants']/tbody/tr
