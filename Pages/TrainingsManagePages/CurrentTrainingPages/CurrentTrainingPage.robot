*** Settings ***
Resource           ../../../Utils/BrowserUtils.robot

*** Variables ***
${I_AM_SURE_BUTTON_LOCATOR}     //input [@value="OK"]
${CURRENT_STATUS_LOCATOR}     //span[contains(@class,'status')]
${DISTRIBUTION_TAB_LOCATOR}     //nav/a[text()='Distribution']
${ADD_SELECTED_STUDENT_BUTTON_LOCATOR}     //input[@title="Add selected student"]
${TRAINING_SHOUND_CONTAIN_GROUPS_POPUP_MESSAGE_LOCATOR}     //span[contains(text(),'Training should contain')]

*** Keywords ***
