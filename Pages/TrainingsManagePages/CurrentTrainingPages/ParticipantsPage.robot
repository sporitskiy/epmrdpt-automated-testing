*** Settings ***
Resource           ../../../Utils/BrowserUtils.robot

*** Variables ***
${SELECT_STUDENT_LOCATOR}     //span[text()='Select student']
${SELECT_STUDENT_FILTER_INPUT_LOCATOR}     //div[@class="chzn-search"]/input
${SEARCH_RESULT_LOCATOR}     //ul[@class="chzn-results"]/li[1]
${ADD_BUTTON_LOCATOR}     //button[@title="Add selected student"]
${THIRD_CHECKBOX_IN_ADDMANY_LOCATOR}     //table[@class="dataTable add-many-table"]/tbody/tr[3]//input
${THIRD_STUDENT_IN_ADDMANY_LOCATOR}     //table[@class="dataTable add-many-table"]/tbody/tr[3]/td[3]
${ADD_MANY_PARTICIPANTS_BUTTON_LOCATOR}     //button[@title="Add many students"]
${ADD_AND_CLOSE_BUTTON_LOCATOR}     //input[@value="Add and Close"]
${ADDMANY_NAME_FILTER_LOCATOR}     //input[@id='tableFilter']
${STUDENTS_IN_PARTIPICANTS_TAB_LOCATORS}     //tbody[@class="available-for-action"]/tr
${CONFIRM_DELETE_STUDENT_BUTTON_LOCATOR}     //p[contains(text(),'Are you sure')]/..//input[@value="Delete"]
${50_LINK_LOCATOR}     //a[text()='50']
