*** Settings ***
Resource           ../../../Utils/BrowserUtils.robot

*** Variables ***
${STUDENT_THAT_IS_NOT_IN_GROUP_LOCATOR}     //tbody[@class="available-for-action"]//span[text()='Add to plan']/../../../../../..//a[@data-user-profile]
${NOT_IN_GROUP_STATUS_LOCATOR}     //tbody[@class="available-for-action"]//span[text()='Add to plan']
${NOT_IN_GROUP_STATUS_SELECT_LOCATOR}     //tbody[@class="available-for-action"]//span[text()='Add to plan']/..//select
