*** Settings ***
Resource           ../../../Utils/BrowserUtils.robot

*** Variables ***
${ADD_GROUP_BUTTON_LOCATOR}     //button[@title="Add new group for current training"]
${X_BUTTON_LOCATOR}     //div[@class="js-group-container"]/div[3]//button[@title="Delete group"]
${CONFIRM_DELETE_GROUP_BUTTON_LOCATOR}     //p[contains(text(),'Are you sure, you want to delete')]/../..//input[@value="Delete"]
${FIRST_GROUP_LINK_LOCATOR}     //div[@class="js-group-container"]/div[2]//a
${GROUPS_COUNTER_LOCATOR}     //div[@class="viewedInfo"]
${FIRST_GROUP_NAME_LOCATOR}     //div[@class="js-group-container"]/div[2]//a/span/span[2]
