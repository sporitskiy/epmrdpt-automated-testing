*** Settings ***
Suite Setup        Login as Student
Suite Teardown     Log Out
Resource           ../../Services/UI/LoginService.robot
Resource           ../../Services/UI/StudentRegistrationService.robot
Resource           ../../GlobalConfig/config.robot

*** Test Cases ***
Registration to Trainings Test
     [Tags]     Important
     Open Trainings Page
     Register to Staffing Training as newcomer
     Student status is     New
     Back
     Register to Planned Training
     Student status is     New
     Open Applications Page
     Trainings are displayed     2

Cancel Registration Test
     Open Trainings Page
     Cancel Training
     Student status is     Not selected
     Restore Registration
     Student status is     New
