*** Settings ***
Suite Setup        Login as Trainer
Suite Teardown     Log Out
Resource           ../../Services/UI/LoginService.robot
Resource           ../../Services/UI/TrainingManageService.robot
Resource           ../../Services/UI/StudentsManageService.robot
Resource           ../../GlobalConfig/config.robot

*** Variables ***

*** Test Cases ***
Finishing the group Test
     [Tags]     Important
     Open Test Group
     Set Students as Finished Training
     Set Group Status     Finished
     Group Status is     Finished

Graduates Test
     Open Graduates Page
     #Select Filter
     Xpath Should Match X Times     //*[@id='graduatesDataTable']/tbody//a     10

Reports Test
     Open Reports Page
     Select Group report
     Check Group Link
     Check Group Members
     View Participants report
     Check Student Link

*** Keywords ***
Select Group report
     Select From List     //select[@id='ReportPath']     GroupsReport
     #Execute Javascript     javascript:$get('ReportViewer_AsyncWait').control._cancelCurrentPostback();
     #Click Element     //*[@id='ReportViewer_AsyncWait_Wait']/table/tbody/tr/td[2]/div/a
     Click     //input[@id='ReportViewer_ctl04_ctl03_ddDropDownButton']
     Drag And Drop     //div[@id='ReportViewer_ctl04_ctl03_divDropDown']//img     //tbody/tr[10]/td[6]/div
     Click     //label[text()='(Select All)']/../input
     Click     //label[contains(text(),'Powers')]/../input
     Click     //input[@value="View Report"]

Check Group Link
     ${window_title}=     Get Title
     Click     //tbody/tr[4]/td[10]/div/a
     Page is opened     Edit training
     Select Window     ${window_title}

Check Group Members
     Click     //tbody/tr[4]/td[11]/div/a
     Xpath Should Match X Times     //div[text()='${student_user_name}']     1

View Participants report
     Select From List     //select[@id='ReportPath']     ParticipantsReport
     ${group_id}=     Remove String Using Regexp     ${group_url}     https://evbyminsd1706.minsk.epam.com/Plan/\\d+/Group/Index/
     Type     //input[@id='ReportViewer_ctl04_ctl03_txtValue']     ${group_id}
     Click     //input[@value="View Report"]     #${VIEW_REPORT_BUTTON_LOCATOR}

Check Student Link
     Click     //tbody/tr[1]/td/table/tbody/tr[3]/td[9]/div/a
     Page is opened     Student profile

Select Filter
     Select From List     //*[@id='TypeId']     Jedi Powers
     Click     //span[@title="Apply filter"]
     Wait Until Page Does Not Contain Element     //img[@src="/Content/ico/preloader.gif"]     5m
