*** Settings ***
Test Teardown      Log Out
Resource           ../../Services/UI/LoginService.robot
Resource           ../../Services/UI/LearningService.robot
Resource           ../../Services/UI/NotificationService.robot
Resource           ../../GlobalConfig/config.robot

*** Test Cases ***
Add Lesson Test
     [Tags]     Important
     Login as Trainer
     Open Learning Page
     Find Learning Group
     Add Lesson
     Open Tab     Schedule
     Lesson is in Calendar

Add Task Test
     [Tags]     Important
     Login as Trainer
     Open Learning Group
     Open Tab     Tasks journal
     Add Task
     Task is added

Add Online task Test
     [Tags]     Important
     Login as Trainer
     Open Learning Group
     Open Tab     Online tasks
     Add Online Task
     Online task is added
     Assign Online Task
     Onlite task is assigned to student

Check Lesson Test
     Login as Student
     Open Learning Page
     Student can see Lesson

Check Task Test
     Login as Student
     Open Learning Page
     Open Tab     Tasks
     Student can see Task

Submit Online Task Test
     [Tags]     Important
     Login as Student
     Open Learning Page
     Submit Online Task
     Task status is     Submitted

Reject Online Task Test
     [Tags]     Important
     Login as Trainer
     Reject online task
     Online task status is     Rejected

Approve Online Task Test
     [Tags]     Important
     Login as Trainer
     Approve online task
     Online task status is     Passed

Student checking Mark is Visible Test
     Login as Student
     Open Learning Page
     Open Tab     Online tasks
     Mark is Visible

Student checking Mark is Not Visible Test
     Login as Trainer
     Change marks visibility
     Log Out
     Login as Student
     Open Learning Page
     Open Tab     Online tasks
     Mark is Not Visible

Send Mail to Student Test
     Login as Trainer
     Open Learning Group
     Open Tab     Students
     Send Mail to Student
     #Outlook is opened

Adding a Notification Test
     Login as Trainer
     Clear notifications
     Open Learning Group
     Change Lesson
     Notification is received

Student checking Notification Test
     Login as Student
     Notification is received
