*** Settings ***
Suite Setup        Login as Trainer
Suite Teardown     Log Out
Resource           ../../Services/UI/LoginService.robot
Resource           ../../Services/UI/TrainingManageService.robot
Resource           ../../Services/UI/StudentsManageService.robot
Resource           ../../GlobalConfig/config.robot

*** Test Cases ***
Create Training Test
     [Tags]     Important
     Open Training Management Page
     Create New Training
     Return to Training Management Page
     Find Training
     Training Status is     Draft

Training Workflow Test
     [Tags]     Important
     Open Test Training
     Set Training Status     Plan
     Training Status is     Planned
     Allow self-registration
     Set Training Status     Staffing
     Training Status is     Staffing
     Set Training Status     Staffed
     Training Status is     Staffing
     Set Training Status     Start
     Training Status is     Staffing
     Set Training Status     Cancel
     Training Status is     Canceled
     [Teardown]     Set Training Status     Staffing

Adding Participants Test
     [Tags]     Important
     Open Test Training
     Add Participant
     Student status is     New

Adding Many Participants Test
     Open Test Training
     Add Many Participants
     Student status is     New

Adding Several Groups Test
     [Tags]     Important
     Open Test Training
     Add Groups     3
     Groups Added

Delete Group Test
     Open Test Training
     Detete Group
     Group Deleted
