*** Settings ***
Resource           ../../Services/UI/LoginService.robot
Resource           ../../GlobalConfig/config.robot

*** Test Cases ***
Registration Test
     [Tags]     Important
     Create New User
     Register new user
     Sleep     15     #Click registration link manually

Change Password Test
     Login as Student
     Check Language
     Change Password
     Seeing Message     Password has been successfully changed
     Log Out
     Login as Student
     Logged to portal
     [Teardown]     Log Out

Reset Password Test
     Reset Password
     Seeing Message     An email with a link to change password has been sent to ${STUDENT_USER_NAME}.
     #TODO

Successfull Login Test
     Login with correct account
     Logged to portal
     [Teardown]     Log Out

*** Keywords ***
