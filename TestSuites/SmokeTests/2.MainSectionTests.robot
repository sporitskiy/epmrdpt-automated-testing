*** Settings ***
Suite Setup        Login as Student
Suite Teardown     Log Out
Resource           ../../Services/UI/LoginService.robot
Resource           ../../GlobalConfig/config.robot

*** Test Cases ***
News Test
     Open News Page
     Page is opened     News

About Test
     Open About Page
     Page is opened     About

Trainings Test
     Open Trainings Page
     Page is opened     Trainings

*** Keywords ***
