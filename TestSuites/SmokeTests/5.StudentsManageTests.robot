*** Settings ***
Suite Setup        Login as Trainer
Suite Teardown     Log Out
Resource           ../../Services/UI/LoginService.robot
Resource           ../../Services/UI/StudentsManageService.robot
Resource           ../../Services/UI/TrainingManageService.robot
Resource           ../../GlobalConfig/config.robot

*** Test Cases ***
Add Students to Group Test
     [Tags]     Important
     Open Test Training
     Accept Students for Training
     Open First Group
     Add Student to group
     Student is added to group
     Add Student to Group with 'Add Many'
     Student is added to group

Add students to group using Distribution Test
     Open Test Training
     Add Student in Distribution
     Student status is     Training In Progress

Add students to group from other training Test
     Open Test Group
     Add Student from Other Training
     Student status is     Training In Progress

Deleting student from group Test
     Open Test Group
     Delete Student from Group
     Open Tab     Participants
     Student status is     Accepted for Training

Setting status Learning to group Test
     [Tags]     Important
     Open Test Group
     #Add Group Trainer
     Set Group Status     Learning
     Group Status is     Learning
