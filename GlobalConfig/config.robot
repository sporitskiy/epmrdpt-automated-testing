*** Settings ***
Library            Selenium2Library
Library            OperatingSystem

*** Variables ***
${BROWSER}         ff
${URL}             https://evbyminsd1706.minsk.epam.com/

*** Keywords ***
Setup
     Set Selenium Timeout     15
     Set Environment Variable     webdriver.chrome.driver     ../chromedriver.exe
     Set Environment Variable     webdriver.ie.driver     ../IEDriverServer.exe
     Open Browser     ${URL}     ${BROWSER}
     Maximize Browser Window
     Sleep     300ms

Go Home
     Go To     ${URL}
