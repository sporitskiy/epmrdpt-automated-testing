*** Settings ***
Library            Selenium2Library

*** Keywords ***
Click
     [Arguments]     ${locator}     ${dont_wait_ajax}=0
     Wait Until Page Contains Element     ${locator}
     Wait Until Element Is Visible     ${locator}
     Click Element     ${locator}
     Run Keyword If     ${dont_wait_ajax}!=0     Sleep     ${dont_wait_ajax}
     ...     ELSE     Wait for Ajax

Type
     [Arguments]     ${locator}     ${text}
     Wait Until Page Contains Element     ${locator}
     Wait Until Element Is Visible     ${locator}
     Input Text     ${locator}     ${text}
     Wait for Ajax

Wait for Ajax
     : FOR     ${INDEX}     IN RANGE     1     51
     \     ${IsAjaxComplete}=     Execute JavaScript     return $.active==0
     \     Run Keyword If     ${IsAjaxComplete}==True     Exit For Loop
     \     Sleep     300ms
     \     Run Keyword If     ${INDEX}==50     Log     Here was an AJAX error.     level=WARN

Open Tab
     [Arguments]     ${tab_name}
     Click     //a[contains(text(),'${tab_name}')]
     Sleep     2

Page is opened
     [Arguments]     ${title}
     Title Should Be     ${title}

Seeing Message
     [Arguments]     ${message}
     Page Should Contain     ${message}

IClick
     [Arguments]     ${locator}     ${expected_element_locator}
     Wait Until Element Is Visible     ${locator}
     Click Element     ${locator}
     Wait Until Page Contains Element     ${expected_element_locator}
     Wait Until Element Is Visible     ${expected_element_locator}

SClick
     [Arguments]     ${locatorToClick}     ${locatorToExpect}=
     ${locatorToExpect}     Set Variable If     "${locatorToExpect}"==""     //span[@class="footerLogo"]     ${locatorToExpect}
     : FOR     ${try}     IN RANGE     0     3
     \     Register Keyword To Run On Failure     Nothing
     \     ${isLocatorToClickVisible}     Run Keyword And Return Status     Wait Until Element Is Visible     ${locatorToClick}     15
     \     Register Keyword To Run On Failure     Capture Page Screenshot
     \     ${isLocatorToClickClicked}     Run Keyword And Return Status     Click Element     ${locatorToClick}
     \     Register Keyword To Run On Failure     Nothing
     \     ${isLocatorToExpectVisible}     Run Keyword And Return Status     Wait Until Element Is Visible     ${locatorToExpect}     15
     \     Register Keyword To Run On Failure     Capture Page Screenshot
     \     Exit For Loop If     '${isLocatorToClickVisible}'=='True' and '${isLocatorToClickClicked}'=='True' and '${isLocatorToExpectVisible}'=='True'
